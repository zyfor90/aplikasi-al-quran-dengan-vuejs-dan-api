// routes.js

import Home from './components/Wrapper.vue';
import Surat from './components/Single.vue';
const routes = [
    { path: '/', component: Home },
    { path: '/surat/:id', component: Surat },
];
export default routes;
